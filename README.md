## **README**

Neste repositório você encontrará informações para replicar o artigo **Dinâmica da inflação para uma economia sob metas de inflação: evidências para o Brasil usando modelos DSGE-VAR**. A documentação é dividida da seguinte forma:

* Em `/ambiente` há o passo a passo de como iniciar uma instância no [Google Cloud](https://cloud.google.com/?hl=pt-br) onde é possível replicar os códigos em R que resolvem, simulam e estimam os modelos DSGE e DSGE-VAR propostos no artigo;
* Em `/desenvolvimento` você encontra os códigos em R que geram as análises e paper (integração do R e LaTeX por meio do [Sweave](https://stat.ethz.ch/R-manual/R-devel/library/utils/doc/Sweave.pdf) e [knitr](https://yihui.name/knitr/))
* Em `/deploy` contém informações de como garantir que a cada nova versão do artigo ele estará disponível para outras pessoas avaliarem.

### **PRÉ-REQUISITOS**

* **Ter executado com sucesso as instruções do respositório [environment_config](https://gitlab.com/hudson-shared/environment_config)**