### **CONFIGURAÇÃO DO AMBIENTE**

Aqui, armazeno códigos para automação da criação do ambiente de desenvolvimento. Você encontrará playbooks em [Ansible](https://www.ansible.com/) responsáveis por iniciar o servidor na Google Cloud e pela gestão de sua configuração.

##### **REQUISITOS**

* Vagrant
* Virtual Box
* Git com uma conta GitLab configurada e com permissão de acesso a este repositório
* Iniciar uma conta no Google Cloud

### **PASSO A PASSO MONTAR O AMBIENTE NO GOOGLE CLOUD**

* Crie o diretório `research` na raiz (cd /) do seu computador;
* Clone este repositório (`git clone git@gitlab.com:hudson-papers/dsge-var.git`) no local de seu interesse:
    * Se estiver no Windows use o Git Bash
* Navegue até a pasta `ambiente` onde o projeto foi clonado (`cd dsge-var/ambiente/`);
* Execute `sudo vagrant plugin install vagrant-vbguest` para adicionar plugin de compartilhamento de arquivos com a vm;
* Execute `sudo vagrant box add bento/ubuntu-16.04` para adicionar a box do ubuntu 16-04 (use a opção 2 - virtualbox)
* Execute `sudo vagrant up --provision` para que o Vagrant inicie a vm e faça o provisionamento usando o Ansible.

No final deste processo, você terá uma máquina virtual. Para acessá-la faça `sudo vagrant ssh` e o terminal da máquina virtual será disponibilizado. Abandone o terminal por um instante e siga as instruções para criar sua conta no Google Cloud.

##### **CONTA DO GOOGLE CLOUD**

Para conseguir replicar o artigo em um servidor do Google Cloud, você precisa obter algunas credenciais da seguinte forma:

* Criar uma conta no Google Cloud neste [link](https://cloud.google.com/?hl=pt-br). Você pode testar o serviço e ganhar alguns créditos;
* Criar um projeto para o artigo (nome sugerido: `dsgevar`);
* Criar uma [conta de serviço](https://developers.google.com/identity/protocols/OAuth2ServiceAccount#creatinganaccount)
* Faça o download das credenciais no formato JSON;
* Salve o arquivo JSON em `/research/cloud-private-pem/`

##### **INICIANDO O SERVIDOR NO GOOGLE CLOUD**

Retorne ao terminal da máquina virtual e navegue até `/research/projetos/dsge-var/` e execute os comandos abaixo:

```
ssh-keygen -t rsa -f ~/.ssh/dsgevar -C "ubuntu"               # Gerar as chaves SSH que serão usadas para conectar ao servidor    
ansible-playbook -i hosts/google google-launch-instance.yml 
```


